<?php

require_once __DIR__ . '/vendor/autoload.php';

use Laravel\Lumen\Application;
$soup = 'chicken';

$app = new Application(realpath(__DIR__ . '.'));

$app->get('/', function () use ($soup) {
    return $soup;
});

$app->run();
